package sample.model;


public class Cliente {
    private int id_cliente;
    private String primer_apellido;
    private String segundo_apellido;
    private String primer_nombre;
    private String otros_nombres;
    private String pais_empleo;
    private String tipo_identificacion;
    private String numero_identificacion;
    private String correo_electronico;
    private String fecha_ingreso;
    private String area;
    private String estado;
    private String fecha_registro;

    public Cliente(int id_cliente, String primer_apellido, String segundo_apellido, String primer_nombre, String otros_nombres, String pais_empleo, String tipo_identificacion, String numero_identificacion, String correo_electronico, String fecha_ingreso, String area, String estado, String fecha_registro) {
        this.id_cliente = id_cliente;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.primer_nombre = primer_nombre;
        this.otros_nombres = otros_nombres;
        this.pais_empleo = pais_empleo;
        this.tipo_identificacion = tipo_identificacion;
        this.numero_identificacion = numero_identificacion;
        this.correo_electronico = correo_electronico;
        this.fecha_ingreso = fecha_ingreso;
        this.area = area;
        this.estado = estado;
        this.fecha_registro = fecha_registro;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundo_apellido() {
        return segundo_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }

    public String getPrimer_nombre() {
        return primer_nombre;
    }

    public void setPrimer_nombre(String primer_nombre) {
        this.primer_nombre = primer_nombre;
    }

    public String getOtros_nombres() {
        return otros_nombres;
    }

    public void setOtros_nombres(String otros_nombres) {
        this.otros_nombres = otros_nombres;
    }

    public String getPais_empleo() {
        return pais_empleo;
    }

    public void setPais_empleo(String pais_empleo) {
        this.pais_empleo = pais_empleo;
    }

    public String getTipo_identificacion() {
        return tipo_identificacion;
    }

    public void setTipo_identificacion(String tipo_identificacion) {
        this.tipo_identificacion = tipo_identificacion;
    }

    public String getNumero_identificacion() {
        return numero_identificacion;
    }

    public void setNumero_identificacion(String numero_identificacion) {
        this.numero_identificacion = numero_identificacion;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(String fecha_registro) {
        this.fecha_registro = fecha_registro;
    }
}

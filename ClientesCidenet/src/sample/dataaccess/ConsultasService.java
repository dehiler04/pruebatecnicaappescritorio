package sample.dataaccess;

import javafx.collections.ObservableList;
import sample.model.Cliente;

import java.time.LocalDate;

public interface ConsultasService {

    public ObservableList<Cliente> consultaUsuarios(String primerNombre, String otrosNombres, String primerApellido,
                                                    String segundoApellido, String tipoIdentificacion, String numeroIdentificacion,
                                                    String paisEmpleo, String correo, String estado);

    public void guardarCliente(String primer_apellido, String segundo_apellido, String primer_nombre,
                               String otros_nombres, String pais_empleo, String tipo_identificacion,
                               String numero_identificacion, String correo_electronico, LocalDate fecha_ingreso,
                               String area, String estado, String fecha_registro);

    public void eliminarCliente(int id);

    public void editarCliente(int id, String primer_apellido, String segundo_apellido, String primer_nombre,
                              String otros_nombres, String pais_empleo, String tipo_identificacion,
                              String numero_identificacion, String correo_electronico, LocalDate fecha_ingreso,
                              String area, String estado, String fecha_registro);

    public int identificacionRepetida(String identificacion);

    public int correoRepetido(String correo);
}

package sample.dataaccess;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.connection.ConnectionBD;
import sample.model.Cliente;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsultasImplementation implements ConsultasService {

    public ObservableList consultaUsuarios(String primerNombre, String otrosNombres, String primerApellido,
                                           String segundoApellido, String tipoIdentificacion, String numeroIdentificacion,
                                           String paisEmpleo, String correo, String estado) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        DateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat df1 = new SimpleDateFormat("yyyy/MM/dd");
        DateFormat df2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        String consulta = "Select * From cliente c" +
                " Where 1 = 1 ";
        if (primerNombre != null && !primerNombre.equals("")) {
            consulta = consulta + " And c.primer_nombre = '" + primerNombre + "' ";
        }
        if (otrosNombres != null && !otrosNombres.equals("")) {
            consulta = consulta + " And c.otros_nombres = '" + otrosNombres + "' ";
        }
        if (primerApellido != null && !primerApellido.equals("")) {
            consulta = consulta + " And c.primer_apellido = '" + primerApellido + "' ";
        }
        if (segundoApellido != null && !segundoApellido.equals("")) {
            consulta = consulta + " And c.segundo_apellido = '" + segundoApellido + "' ";
        }
        if (tipoIdentificacion != null && !tipoIdentificacion.equals("")) {
            consulta = consulta + " And c.tipo_identificacion = '" + tipoIdentificacion + "' ";
        }
        if (numeroIdentificacion != null && !numeroIdentificacion.equals("")) {
            consulta = consulta + " And c.numero_identificacion = '" + numeroIdentificacion + "' ";
        }
        if (paisEmpleo != null && !paisEmpleo.equals("")) {
            consulta = consulta + " And c.pais_empleo = '" + paisEmpleo + "' ";
        }
        if (correo != null && !correo.equals("")) {
            consulta = consulta + " And c.correo_electronico = '" + correo + "' ";
        }
        if (estado != null && !estado.equals("")) {
            consulta = consulta + " And c.estado = '" + estado + "' ";
        }

        ObservableList<Cliente> listview = FXCollections.observableArrayList();
        try {
            ConnectionBD cn = new ConnectionBD();
            Connection cn1 = cn.fileconnection();

            PreparedStatement ps = cn1.prepareStatement(consulta);
            ResultSet r = ps.executeQuery();
            while (r.next()) {
                listview.add(new Cliente(r.getInt("id_cliente"), r.getString("primer_apellido"),
                        r.getString("segundo_apellido"), r.getString("primer_nombre"),
                        r.getString("otros_nombres"), r.getString("pais_empleo"),
                        r.getString("tipo_identificacion"), r.getString("numero_identificacion"),
                        r.getString("correo_electronico"), dateFormat2.format(df1.parse(r.getString("fecha_ingreso").replace("-", "/"))),
                        r.getString("area"), r.getString("estado"), dateFormat.format(df2.parse(r.getString("fecha_registro").replace("-", "/")))));
            }
            ps.close();

        } catch (Exception e) {
            Logger.getLogger(ConsultasImplementation.class.getName()).log(Level.SEVERE, null, e);
        }
        return listview;
    }

    @Override
    public void guardarCliente(String primer_apellido, String segundo_apellido, String primer_nombre,
                               String otros_nombres, String pais_empleo, String tipo_identificacion,
                               String numero_identificacion, String correo_electronico, LocalDate fecha_ingreso,
                               String area, String estado, String fecha_registro) {
        DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        try {
            String sql = "insert into cliente (primer_apellido,segundo_apellido,primer_nombre,otros_nombres,pais_empleo,tipo_identificacion,numero_identificacion,correo_electronico,fecha_ingreso,area,estado,fecha_registro)" +
                    "values" +
                    "('" + primer_apellido + "', '" + segundo_apellido + "', '" + primer_nombre + "', '" + otros_nombres + "', " +
                    "'" + pais_empleo + "', '" + tipo_identificacion + "', '" + numero_identificacion + "', '" + correo_electronico + "', '" + LocalDate.parse(fecha_ingreso.toString(), df1) +
                    "', '" + area + "', '" + estado + "', '" + dateFormat.format(df2.parse(fecha_registro)).toString().replace("/", "-") + "');";

            ConnectionBD cn = new ConnectionBD();
            Connection cn1 = cn.fileconnection();

            PreparedStatement ps = cn1.prepareStatement(sql);
            ps.execute();
            ps.close();

        } catch (Exception e) {
            Logger.getLogger(ConsultasImplementation.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    @Override
    public void eliminarCliente(int id) {
        String sql = "Delete from cliente c where c.id_cliente = " + id;

        try {
            ConnectionBD cn = new ConnectionBD();
            Connection cn1 = cn.fileconnection();

            PreparedStatement ps = cn1.prepareStatement(sql);
            ps.execute();
            ps.close();

        } catch (Exception e) {
            Logger.getLogger(ConsultasImplementation.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void editarCliente(int id, String primer_apellido, String segundo_apellido, String primer_nombre, String otros_nombres, String pais_empleo, String tipo_identificacion, String numero_identificacion, String correo_electronico, LocalDate fecha_ingreso, String area, String estado, String fecha_registro) {

        DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        try {

            String sql = "Update Cliente " +
                    "Set primer_apellido = '" + primer_apellido + "'," +
                    "segundo_apellido = '" + segundo_apellido + "'," +
                    "primer_nombre = '" + primer_nombre + "'," +
                    "otros_nombres = '" + otros_nombres + "'," +
                    "pais_empleo = '" + pais_empleo + "'," +
                    "tipo_identificacion = '" + tipo_identificacion + "'," +
                    "numero_identificacion = '" + numero_identificacion + "'," +
                    "correo_electronico = '" + correo_electronico + "'," +
                    "fecha_ingreso = '" + LocalDate.parse(fecha_ingreso.toString(), df1) + "'," +
                    "area = '" + area + "'," +
                    "estado = '" + estado + "'," +
                    "fecha_registro = '" + dateFormat.format(df2.parse(fecha_registro)).toString().replace("/", "-") + "'" +
                    "Where id_cliente = " + id;

            ConnectionBD cn = new ConnectionBD();
            Connection cn1 = cn.fileconnection();

            PreparedStatement ps = cn1.prepareStatement(sql);
            ps.execute();
            ps.close();

        } catch (Exception e) {
            Logger.getLogger(ConsultasImplementation.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public int identificacionRepetida(String identificacion) {

        int resultado = 0;
        String sql = "Select NVL(Count(numero_identificacion), 0) numero From cliente " +
                "where numero_identificacion = '" + identificacion + "'";

        try {
            ConnectionBD cn = new ConnectionBD();
            Connection cn1 = cn.fileconnection();

            PreparedStatement ps = cn1.prepareStatement(sql);
            ResultSet r = ps.executeQuery();
            while (r.next()) {
                resultado = r.getInt("numero");
            }
            ps.close();

        } catch (Exception e) {
            Logger.getLogger(ConsultasImplementation.class.getName()).log(Level.SEVERE, null, e);
        }
        return resultado;
    }

    @Override
    public int correoRepetido(String correo) {
        int resultado = 0;
        String sql = "Select NVL(Count(correo_electronico), 0) numero From cliente " +
                "where correo_electronico = '" + correo + "'";

        try {
            ConnectionBD cn = new ConnectionBD();
            Connection cn1 = cn.fileconnection();

            PreparedStatement ps = cn1.prepareStatement(sql);
            ResultSet r = ps.executeQuery();
            while (r.next()) {
                resultado = r.getInt("numero");
            }
            ps.close();

        } catch (Exception e) {
            Logger.getLogger(ConsultasImplementation.class.getName()).log(Level.SEVERE, null, e);
        }
        return resultado;
    }


}

package sample.connection;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionBD {
    public  String namedatabase = "cidenetdatabase";
    private String url = "jdbc:h2:./database/"+namedatabase+";IFEXISTS=TRUE";
    private String user = "sa";
    private String password = "";

    public Connection fileconnection(){
        try {
            Class.forName("org.h2.Driver");
            Connection connectdata = DriverManager.getConnection(url, user, password);
            System.out.println("SUCCES");
            return  connectdata;
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "ERROR" + ex.getMessage());
            return  null;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"ERROR" + ex.getMessage());
            return  null;
        }
    }

}

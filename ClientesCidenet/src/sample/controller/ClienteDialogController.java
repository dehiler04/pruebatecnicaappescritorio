package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import sample.dataaccess.ConsultasImplementation;
import sample.model.Cliente;


import java.awt.*;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class ClienteDialogController implements Initializable {

    @FXML
    private TextField txpapellido;
    @FXML
    private TextField txsapellido;
    @FXML
    private TextField txpnombre;
    @FXML
    private TextField txonombres;
    @FXML
    private ComboBox<String> txtident;
    @FXML
    private TextField txidentifi;
    @FXML
    private TextField txcorreo;
    @FXML
    private DatePicker txfechaing;
    @FXML
    private ComboBox<String> txarea;
    @FXML
    private ComboBox<String> txestado;
    @FXML
    private TextField txfechareg;
    @FXML
    private ComboBox<String> txpais;
    @FXML
    private Button btnguardar;
    @FXML
    private Label labelfechareg;

    public boolean editar = false;
    boolean todoOk = true;
    private int id_cliente;
    private String numeroidentificacion = "";


    ObservableList<String> tiposIdentificaciones = FXCollections.observableArrayList("Cedula de Ciudadania", "Cedula de Extranjeria",
            "Targeta de Identidad", "Registro Civil");

    ObservableList<String> paises = FXCollections.observableArrayList("Colombia", "Estados Unidos");

    ObservableList<String> areas = FXCollections.observableArrayList("Administracion", "Financiera", "Compras", "Infraestructura", "Operacion", "Talento Humano",
            "Servicios Varios");
    ObservableList<String> estados = FXCollections.observableArrayList("Activo", "Inactivo");

    ConsultasImplementation consultas;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtident.setItems(tiposIdentificaciones);
        txpais.setItems(paises);
        txarea.setItems(areas);
        txestado.setItems(estados);
        txestado.getSelectionModel().select("Activo");
        txpais.getSelectionModel().select("Colombia");
        txtident.getSelectionModel().select("Cedula de Ciudadania");
        txarea.getSelectionModel().select("Administracion");
        txestado.setDisable(true);
        txfechareg.setText(fechaActual());
        consultas = new ConsultasImplementation();
        LocalDate maxDate = LocalDate.now();
        txfechaing.setValue(maxDate);
        txfechaing.getEditor().setDisable(true);
        restrictDatePicker(txfechaing, maxDate.minusMonths(1), maxDate);

    }

    public void restrictDatePicker(DatePicker datePicker, LocalDate minDate, LocalDate maxDate) {
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(minDate)) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        } else if (item.isAfter(maxDate)) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePicker.setDayCellFactory(dayCellFactory);
    }

    public void inicializarAtributos(Cliente cliente, Boolean isNew) {
        if (!isNew) {
            DateTimeFormatter dateFormat2 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            txpapellido.setText(cliente.getPrimer_apellido());
            txsapellido.setText(cliente.getSegundo_apellido());
            txpnombre.setText(cliente.getPrimer_nombre());
            txonombres.setText(cliente.getOtros_nombres());
            txtident.getSelectionModel().select(cliente.getTipo_identificacion());
            txidentifi.setText(cliente.getNumero_identificacion());
            txcorreo.setText(cliente.getCorreo_electronico());
            txfechaing.setValue(LocalDate.parse(cliente.getFecha_ingreso(), dateFormat2));
            txarea.getSelectionModel().select(cliente.getArea());
            txestado.getSelectionModel().select(cliente.getEstado());
            txfechareg.setText(fechaActual());
            txpais.getSelectionModel().select(cliente.getPais_empleo());
            txestado.setDisable(false);
            labelfechareg.setText("Fecha Modificación");
            id_cliente = cliente.getId_cliente();
            txfechaing.getEditor().setDisable(true);
            editar = true;
            numeroidentificacion = cliente.getNumero_identificacion();
        }
    }

    public String fechaActual() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void validarPimerApellido(KeyEvent event) {
        if (txpapellido.getText().length() >= 20) {
            event.consume();
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public void validarSegundoApellido(KeyEvent event) {
        if (txsapellido.getText().length() >= 20) {
            event.consume();
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public void validarPrimerNombre(KeyEvent event) {
        if (txpnombre.getText().length() >= 20) {
            event.consume();
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public void validarOtroNombres(KeyEvent event) {
        if (txonombres.getText().length() >= 50) {
            event.consume();
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public void validarCamposDelNombre(String texto, String elemento) {
        Pattern patron = Pattern.compile("[A-Z ]+");
        if (!patron.matcher(texto).matches() && todoOk) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("ERROR");
            alert.setContentText("El campo " + elemento + " no puede estar vacio y debe ser en Mayúsculas, sin acentos ni numeros ");
            alert.showAndWait();
            todoOk = false;
        }

    }

    public void validarCamposNumericos(String texto, String elemento) {
        Pattern patron = Pattern.compile("[a-zA-Z0-9-]+");

        if (!patron.matcher(texto).matches() && todoOk) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("ERROR");
            alert.setContentText("El campo " + elemento + " no puede estar vacio, puede contener los siguientes elementos: a-z, A-Z, 0-9 y -, sin acentos");
            alert.showAndWait();
            todoOk = false;
        } else {
            if (!numeroidentificacion.equals(txidentifi.getText())) {
                if (consultas.identificacionRepetida(texto) > 0 && todoOk) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText(null);
                    alert.setTitle("ERROR");
                    alert.setContentText("El campo " + elemento + " se encuentra repetido.");
                    alert.showAndWait();
                    todoOk = false;
                }
            }
        }

    }

    public void crearCorreo() {
        String correo = txpnombre.getText().trim().toLowerCase() + "." + txpapellido.getText().trim().toLowerCase().replace(" ", "");
        if (txpais.getSelectionModel().getSelectedItem() != null && txpais.getSelectionModel().getSelectedItem().trim().equals("Colombia")) {
            int cantRepetido = consultas.correoRepetido(correo + "@cidenet.com.co");
            if (cantRepetido > 0) {//if para validar si existen otros con el mismo correo
                correo = correo + "." + cantRepetido;
            }
            correo = correo + "@cidenet.com.co";
        } else {
            int cantRepetido = consultas.correoRepetido(correo + "@cidenet.com.us");
            if (cantRepetido > 0) {//if para validar si existen otros con el mismo correo
                correo = correo + "." + cantRepetido;
            }
            correo = correo + "@cidenet.com.us";
        }
        txcorreo.setText(correo);
        ;
    }

    public void validaryguardar(ActionEvent event) {
        Controller con = new Controller();
        validarCamposDelNombre(txpapellido.getText().trim(), "Primer Apellido");
        validarCamposDelNombre(txsapellido.getText().trim(), "Segundo Apellido");
        validarCamposDelNombre(txpnombre.getText().trim(), "Primer Nombre");
        validarCamposDelNombre(txonombres.getText().trim(), "Otros Nombres");
        validarCamposNumericos(txidentifi.getText().trim(), "Número Identificación");
        crearCorreo();
        if (todoOk) {
            if (editar) {
                editarUsuarioFin();
            } else {
                guardarUsuarioFin();
            }
        }
        todoOk = true;
    }

    public void guardarUsuarioFin() {
        consultas.guardarCliente(txpapellido.getText().trim(), txsapellido.getText().trim(), txpnombre.getText().trim(),
                txonombres.getText().trim(), txpais.getSelectionModel().getSelectedItem(), txtident.getSelectionModel().getSelectedItem(),
                txidentifi.getText(), txcorreo.getText(), txfechaing.getValue(),
                txarea.getSelectionModel().getSelectedItem(), txestado.getSelectionModel().getSelectedItem(), txfechareg.getText());

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText("Se ha agregado correctamente el usuario");
        alert.showAndWait();
        Stage stage = (Stage) btnguardar.getScene().getWindow();
        stage.close();
    }

    public void editarUsuarioFin() {
        consultas.editarCliente(id_cliente, txpapellido.getText().trim(), txsapellido.getText().trim(), txpnombre.getText().trim(),
                txonombres.getText().trim(), txpais.getSelectionModel().getSelectedItem(), txtident.getSelectionModel().getSelectedItem(),
                txidentifi.getText(), txcorreo.getText(), txfechaing.getValue(),
                txarea.getSelectionModel().getSelectedItem(), txestado.getSelectionModel().getSelectedItem(), txfechareg.getText());

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Información");
        alert.setContentText("Se ha editado correctamente el usuario");
        alert.showAndWait();
        Stage stage = (Stage) btnguardar.getScene().getWindow();
        stage.close();
    }

}

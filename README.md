# **Experiencia Gestión De Usuarios** - Cidenet

## Introducción 🚀
_Gestión de Usuarios es un Sistema que permite registrar el ingreso y la salida de los empleados, así como administrar su información._

## Pre-requisitos 📋
- Java v1.8 update 151 o superior y JDK (variables de entorno configuradas)
- IntelliJ IDEA (2019.3)
- Sistema administrador de base de datos H2
- JavaFx

## Instalación 🔧
- Para clonar este repositorio localmente, se debe ejecutar el siguiente comando: 
```git clone https://dehiler04@bitbucket.org/dehiler04/pruebatecnicaappescritorio.git``` 
- Importar proyecto en IntelliJ IDE. 
- Configurar JRE System Library con JavaSE-1.8
- Configurar la codificación a UTF-8 al proyecto una vez sea importado
- Instalación de librería (driver) de H2 para reconocimiento de Base de Datos ubicado en la carpeta H2 -> bin -> h2-1.3.174.jar
	dentro del zip que se encuentra en el repositorio (Gestor de base de datos).

## Para Ejecutar la Aplicación ⚙️ 
- Para correr el proyecto se necesita Java JDK 1.8.

🚧 La estructura completa del proyecto es la siguiente:

* ```src/sample```
``` 
+ connection
    Clases que permiten la conexión a base de datos H2.

+controller
    Clases encargadas de controlar eventos y mapeo de elementos front.
 
+ dataaccess
    Clases que realizan las acciones de acceso a datos: consulta, insert, update y delete.

+ images
    Imagenes necesarias para la visualización en front. 

+ model
    Clases para estructurar elementos de base de datos en objetos java.    

+ view
    Archivos formato Fxml encargados de los componentes de visualización del aplicativo.

+ 
    Clase Main: Encargada de lanzamiento del aplicativo.
```


##Construido Con 🛠️
 La Aplicación fue desarrollada con:
 - JavaFX 
 - Base de datos H2
 - Lenguaje de programación Java


## Versionado 📌 
Se usó GIT para el control de versiones, aplicando Git flow🔀 

## Autores ✒️
* **Dehiler Manuel Sepulveda Vargas**
[manueldmsv@gmail.com]